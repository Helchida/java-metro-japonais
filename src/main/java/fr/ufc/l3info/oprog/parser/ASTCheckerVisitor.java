package fr.ufc.l3info.oprog.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Vérification du fichier parsé :
 *  - liste de lignes non vide
 *  - nom de ligne vide
 *  - nom de stations non vides
 *  - noms de stations uniques sur une ligne
 *  - distance kilométrique positive
 *  - distance kilométrique strictement croissante à partir de 0
 *  - toutes les lignes sont accessibles
 */
public class ASTCheckerVisitor implements ASTNodeVisitor {

    private HashMap<String, ERROR_KIND> errors;

    public ASTCheckerVisitor() {
        errors = new HashMap<>();
    }

    public Map<String, ERROR_KIND> getErrors() {
        return new HashMap<>(errors);
    }

    public ArrayList<Integer> tab_error_wrong_number = new ArrayList<Integer>();


    @Override
    public Object visit(ASTNode n) {

        n.accept(this);
        return null;
    }

    @Override
    public Object visit(ASTListeLignes n) {
        if(n.children.size()==0){
            String str_error="Empty_File_"+n.ligne+"_"+n.colonne;
            errors.put(str_error,ERROR_KIND.EMPTY_LINE_LIST);
        }
        ArrayList<String> listeStation = new ArrayList<>();
        int cpt_intersection=1;
        ArrayList arr =new ArrayList<>();
        for (ASTNode child : n.children) {
            int cpt=0;
            for (ASTNode child_bis : n.children){
                if((child.children.get(0).value).equals(child_bis.children.get(0).value)){
                    cpt++;
                }
            }
            if(cpt>1){
                if(arr.contains(child.children.get(0).value)) {
                    String str_error = "Same_Name_Line_" + child.ligne + "_" + child.colonne;
                    errors.put(str_error, ERROR_KIND.DUPLICATE_LINE_NAME);
                }
            }

            if(!(arr.contains(child.children.get(0).value))){
                arr.add(child.children.get(0).value);
            }
            String s=child.children.get(0).value;
            s=s.substring(1);
            s=s.substring(0,s.length()-1);
            if(s.trim().equals("")){
                String str_error="Empty_Name_Line_"+child.ligne+"_"+child.colonne;
                errors.put(str_error,ERROR_KIND.EMPTY_LINE_NAME);
            }

            boolean has_Intersection=false;
            for(int i=1;i<child.children.size();i++){
                if(!listeStation.contains(child.children.get(i).children.get(0).value)){
                    listeStation.add(child.children.get(i).children.get(0).value);
                }else{
                    has_Intersection=true;
                }
            }
            if(has_Intersection){
                cpt_intersection++;
            }

        }

        System.out.println("child size :"+n.children.size()+"\n");
        System.out.println("cpt intersection :"+cpt_intersection+"\n");
        if(n.children.size()>cpt_intersection && n.children.size()!=0){

            String str_error="Non_Accessible_Line_"+n.ligne+"_"+n.colonne;
            errors.put(str_error,ERROR_KIND.UNREACHABLE_LINE);
        }

        for (ASTNode child : n.children) {
            child.accept(this);
        }

        return null;
    }

    @Override
    public Object visit(ASTLigne n) {
        double number_before=0.0;
        ArrayList arr =new ArrayList<>();
        for(int i=1;i<n.getNumChildren();i++){
            int cpt=0;
            for (int j=1;j<n.getNumChildren();j++){

                    if ((n.children.get(i).children.get(0).value).equals(n.children.get(j).children.get(0).value)) {
                        cpt++;
                    }

            }

            if(cpt>1){
                if(arr.contains(n.children.get(i).children.get(0).value)){

                    String str_error="Same_Name_Station_"+n.children.get(i).ligne+"_"+n.children.get(i).colonne;
                    errors.put(str_error,ERROR_KIND.DUPLICATE_STATION_NAME);
                }

            }

            if(!(arr.contains(n.children.get(i).children.get(0).value))){
                arr.add(n.children.get(i).children.get(0).value);
            }

            if(i==1){
                if(Double.parseDouble(n.children.get(i).children.get(1).value)!=0.0){

                    if(!tab_error_wrong_number.contains(n.children.get(i).ligne)){
                        tab_error_wrong_number.add(n.children.get(i).ligne);
                        String str_error="Not_Zero_For_First_Station_"+n.children.get(i).ligne+"_"+n.children.get(i).colonne;
                        errors.put(str_error,ERROR_KIND.WRONG_NUMBER_VALUE);
                    }





                }
            }else{
                if(Double.parseDouble(n.children.get(i).children.get(1).value)<=number_before){
                    if(!tab_error_wrong_number.contains(n.children.get(i).ligne)) {
                        tab_error_wrong_number.add(n.children.get(i).ligne);
                        String str_error = "Not_Croissant_Order_Number_" + n.children.get(i).ligne + "_" + n.children.get(i).colonne;
                        errors.put(str_error, ERROR_KIND.WRONG_NUMBER_VALUE);
                    }
                }
            }

            number_before=Double.parseDouble(n.children.get(i).children.get(1).value);





            n.children.get(i).accept(this);

        }





        return null;
    }

    @Override
    public Object visit(ASTDeclaration n) {

        n.children.get(0).accept(this);
        n.children.get(1).accept(this);
        return null;
    }

    @Override
    public Object visit(ASTChaine n) {
        String s=n.value;
        s=s.substring(1);
        s=s.substring(0,s.length()-1);
        if(s.trim().equals("")){
            String str_error="Empty_Name_Station_"+n.ligne+"_"+n.colonne;
            errors.put(str_error,ERROR_KIND.EMPTY_STATION_NAME);
        }
        return null;
    }

    @Override
    public Object visit(ASTNombre n) {
        if(Double.parseDouble(n.value)<0){
            if(!tab_error_wrong_number.contains(n.ligne)) {
                tab_error_wrong_number.add(n.ligne);
                String str_error = "Negative_Number_" + n.ligne + "_" + n.colonne;
                errors.put(str_error, ERROR_KIND.WRONG_NUMBER_VALUE);
            }
        }

        return null;
    }
}

enum ERROR_KIND {
    EMPTY_LINE_LIST,
    EMPTY_LINE_NAME,
    DUPLICATE_LINE_NAME,
    EMPTY_STATION_NAME,
    DUPLICATE_STATION_NAME,
    WRONG_NUMBER_VALUE,
    UNREACHABLE_LINE
}