package fr.ufc.l3info.oprog;

public class TicketMachine {

    private static TicketMachine INSTANCE = new TicketMachine();

    private TicketMachine() {
    }

    public static TicketMachine getInstance(){
        return INSTANCE;
    }

    public ITicket buyTicket(boolean child, int... amount){
        if(amount.length==0){
            //Ici on return un nouveau ticket 10 voyages
            return new TenIllimitedTripsTicket(child);
        }
        if(amount.length==1&&amount[0]>0){
            //Ici on return un nouveau base ticket avec le montant spécifié
            return new BaseTicket(child, amount[0]);
        }
        //valeur incorrect dans amount, on return donc null pour spécifier que aucun ticket n'a été créé
        return null;
    }

    public ITicket adjustFare(ITicket old, int amount){
        //Si l'ancien ticket est null on renvoie null
        if(old==null){
            return null;
        }
        //Si le montant n'est pas strictement positif on renvoie l'ancien ticket
        if(!(amount>0)){
            return old;
        }
        if(old instanceof TenIllimitedTripsTicket){
            return  old;
        }
        return new AdjustedTicket(old,amount);

    }


}
