package fr.ufc.l3info.oprog;

import java.util.Map;
import java.util.Objects;

public class Barrier {
    Network net;
    String station;
    Map<Double,Integer> prices;

    private Barrier(Network net, String station, Map<Double,Integer> prices){
        this.net=net;
        this.station=station;
        this.prices=prices;
    }

    public static Barrier build(Network n, String s, Map<Double,Integer> p){
        if(n!=null&&s!=null&&p!=null){
            if(n.getLines().size()!=0){
               if(n.getStationByName(s)!=null){
                    if(p.size()>0){
                        for(Double m : p.keySet()){
                            if(m<0.0){
                                return null;
                            }
                            if(p.get(m)<=0){
                                return null;
                            }
                        }
                        if(p.get(0.0)==null){
                            return null;
                        }
                        return new Barrier(n,s,p);
                    }else{
                        return null;
                    }
               }else{
                   return null;
               }
            }else {
                return null;
            }
        }else{
            return null;
        }
    }

    public boolean enter(ITicket t){ //A CHECK DE DEPLACER DANS LES ENTERING DES TICKETS SAUF LE GETAMOUNT
        return (t.getAmount()>0)&&(t.entering(this.station));

    }

    /*public boolean exit(ITicket t){ //A REVOIR $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        if(!t.isValid() || t.getEntryStation() == null){
            return false;
        }
        Double dist = this.net.distance(t.getEntryStation(),this.station);
        Double tempmin = -1.0;
        for (Double k : this.prices.keySet()) {
            if(dist >= k && tempmin <= k){
                tempmin = k;
            }
        }
        if(t.isChild()){
            if(this.prices.get(tempmin)/2 > t.getAmount()){
                return false;
            }
        }else{
            if(this.prices.get(tempmin) > t.getAmount()){
                return false;
            }
        }
        t.invalidate();
        return true;
    }*/

    public boolean exit(ITicket t){
        if(!t.isValid() || t.getEntryStation() == null){
            return false;
        }
        int price = 0;
        Double distance = this.net.distance(t.getEntryStation(),this.station);
        for (Double i : this.prices.keySet()) {
            if(distance >= i && price < prices.get(i)){
                price = prices.get(i);
            }
        }
        if(t.isChild()){
            price/=2;
        }
        if(price > t.getAmount()){
            return false;
        }
        t.invalidate();
        return true;
    }
}
