package fr.ufc.l3info.oprog;

public class TenIllimitedTripsTicket implements ITicket {

    private int nombre_trajets;
    private boolean child;
    private int state;
    private String station_entered;
    final private int ISSUED = 0;
    final private int ENTERED = 1;
    final private int INVALID = 2;

    protected TenIllimitedTripsTicket(boolean child){
        this.nombre_trajets=10;
        this.child=child;
        this.state=ISSUED;
    }
    @Override
    public boolean isChild() {
        return this.child;
    }

    @Override
    public String getEntryStation() {
        if(this.state==ENTERED){
            return this.station_entered;
        }
        return null;
    }

    @Override
    public boolean entering(String name) {
        if(this.nombre_trajets>0 && (state==ISSUED || state==ENTERED)){
            this.state=ENTERED;
            this.station_entered=name;
            this.nombre_trajets--;
            System.out.println("true");
            return true;
        }else{
            this.invalidate();
        }
        System.out.println("false");
        return false;
    }

    @Override
    public void invalidate() {
        if(this.nombre_trajets==0){
            this.state=INVALID;
        }

    }

    @Override
    public int getAmount() {

        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isValid() {
        if(this.state==INVALID){
            return false;
        }
        return true;
    }
}
