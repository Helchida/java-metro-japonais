package fr.ufc.l3info.oprog;

import java.util.*;

/**
 * Le réseau contient un ensemble de stations,
 * organisées en lignes pour lesquelles certaines
 * stations permettent un transfert d'une ligne à l'autre.
 */
public class Network {

    Set<Station> stations;

    /**
     * construit un réseau vide
     */
    public Network(){
        this.stations=new HashSet<>();
    }

    /**
     * permet d'ajouter la station passée en paramètre au réseau.
     * La station ne sera ajoutée au réseau que si une autre station
     * ne porte pas déjà le même nom.
     * @param s
     */
    public void addStation(Station s){
        boolean isPresent = false;
        for(Station st : this.stations){
            if(st.equals(s)){
                isPresent=true;
                break;
            }
        }
        if(!isPresent){
            stations.add(s);
        }
    }

    /**
     * retourne l'ensemble des lignes présentes sur le réseau.
     * @return
     */
    public Set<String> getLines(){
        Set<String> lignes = new HashSet<>();
        for(Station st : this.stations){
            lignes.addAll(st.getLines());
        }
        return lignes;
    }

    /**
     * permet de récupérer une station du réseau à partir de son nom.
     * Si la station n'existe pas, la méthode renvoie une valeur null.
     * @param name
     * @return
     */
    public Station getStationByName(String name) {
        for(Station st : this.stations){
            if(st.getName().equals(name)){
                return st;
            }
        }
        return null;
    }

    /**
     * permet de récupérer une station du réseau à partir de la ligne
     * et du numéro de station. Si la station n'existe pas,
     * la méthode renvoie une valeur null.
     * @param line
     * @param number
     * @return
     */
    public Station getStationByLineAndNumber(String line, int number){
        for(Station st : this.stations){
            if(st.getLines().contains(line)&&(st.getNumberForLine(line)==number)){
                return st;
            }
        }
        return null;
    }

    /**
     * permet de savoir si le réseau est comme valide. Un réseau sera considéré
     * comme valide si toutes les conditions suivantes sont remplies : il existe
     * au moins une ligne, chaque ligne est correctement structurée (chaque ligne
     * contient au moins une station, la première station porte le numéro 1 et se
     * trouve au kilomètre 0, la numérotation des stations de la ligne ne présente
     * pas de numéros en double ou de numéro manquants, et les positions
     * kilométriques sont strictement croissantes tout au long de la ligne).
     * Par ailleurs, chaque station doit être accessible depuis n'importe quelle
     * autre station (dit autrement : il n'y a pas de ligne isolée sur le réseau).
     * @return
     */
    /**
    public boolean isValid(){
        //Au moins une ligne
        if(this.getLines().size()>0){
            //Premiere station de chaque ligne porte le numero 1 et se trouve au kilometre 0
            for(String ligneNetwork : this.getLines()){
                if(getStationByLineAndNumber(ligneNetwork,1)==null||getStationByLineAndNumber(ligneNetwork,1).getDistanceForLine(ligneNetwork)!=0){
                    return false;
                }
            }
            //La numerotation des stations ne présente pas de doublons ou de numero manquant
            for(String ligneNetwork : this.getLines()){
                int i = 0;
                boolean keep=true;
                while(keep){
                    if(getStationByLineAndNumber(ligneNetwork,i)!=null) {
                        if(getStationByLineAndNumber(ligneNetwork,i+1)!=null){
                            i++;
                        }else{
                            if(getStationByLineAndNumber(ligneNetwork,i+2)!=null){
                                return false;
                            }else{
                                keep = false;
                            }
                        }
                    }else{

                    }
                }
            }
        }else{
            return false;
        }

    }*/
    public boolean isValid(){
        if(getLines().size()==0){
            return false;
        }

        for(Station s : stations){
            for(String l : s.getLines()){
                for(Station s2 : stations){
                    if(s.getName()!=s2.getName() && s.getNumberForLine(l)==s2.getNumberForLine(l)){
                        return false;
                    }
                }
                if(s.getNumberForLine(l)==1){
                    if(s.getDistanceForLine(l)!=0.0){
                        return false;
                    }
                }else{
                    if(s.getDistanceForLine(l)==0.0){
                        return false;
                    }
                    if(getStationByLineAndNumber(l,s.getNumberForLine(l)-1)==null){
                        return false;
                    }
                    if(s.getDistanceForLine(l)<=getStationByLineAndNumber(l,s.getNumberForLine(l)-1).getDistanceForLine(l)){
                        return false;
                    }
                }
            }
        }

        for(String li : getLines()){
            boolean have_intersection=false;
            for(Station st : stations){

                if(st.getLines().contains(li)){
                    if(st.getLines().size()>1){
                        have_intersection=true;
                    }
                }
            }
            if(!have_intersection && getLines().size()>1){
                return false;
            }
        }

        return true;
    }
    public  double distance_rec_two(String avant, String temp, String fin, List<String> deja_visit, double dist){
        if(deja_visit.contains(temp)){
            return -1;
        }

        if(temp.equals(fin)){
            return dist;
        }

        deja_visit.add(temp);

        for(String str1 : getStationByName(temp).getLines()){
            Station stat=getStationByName(temp);
            dist += Math.abs(stat.getDistanceForLine(str1)-getStationByName(avant).getDistanceForLine(str1));
            double test1;
            double test2;
            if(stat.getNumberForLine(str1) == 1 && stat.getLines().size() == 1) {
                distance_rec_two(temp,getStationByLineAndNumber(str1,stat.getNumberForLine(str1)+1).getName(),fin,deja_visit,dist);
            }else{
                distance_rec_two(temp,getStationByLineAndNumber(str1,stat.getNumberForLine(str1)+1).getName(),fin,deja_visit,dist);
                distance_rec_two(temp,getStationByLineAndNumber(str1,stat.getNumberForLine(str1)-1).getName(),fin,deja_visit,dist);
            }


            System.out.println(dist );
        }
        return dist;
    }
    public double distance(String s1, String s2){
        for(String str1 : getStationByName(s1).getLines()){
            for(String str2 : getStationByName(s2).getLines()){
                if(str1.equals(str2)){
                    return Math.abs(getStationByName(s1).getDistanceForLine(str1)-getStationByName(s2).getDistanceForLine(str2));
                }
            }
        }

        /*String temp = s1;
        double dist = 0.0;
        List<String> testList = new ArrayList<String>();
        System.out.println("dist = "+distance_rec_two(s1,temp,s2,testList,dist));*/



        return -1.0;
    }
}

