package fr.ufc.l3info.oprog;

public class AdjustedTicket implements ITicket{

    ITicket _original;
    int newAmount;

    protected AdjustedTicket(ITicket initial, int amount){
        if(initial==null){
            throw new NullPointerException();
        }
        _original = initial;
        if(amount>0){
            newAmount=_original.getAmount()+amount;
        }else{
            newAmount=_original.getAmount();
        }
    }

    @Override
    public boolean isChild() {
        return _original.isChild();
    }

    @Override
    public String getEntryStation() {
        return _original.getEntryStation();
    }

    @Override
    public boolean entering(String name) {
        return _original.entering(name);
    }

    @Override
    public void invalidate() {
        _original.invalidate();
    }

    @Override
    public int getAmount() {
        return this.newAmount;
    }

    @Override
    public boolean isValid() {
        return _original.isValid();
    }
}
