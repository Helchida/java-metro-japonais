package fr.ufc.l3info.oprog;


import java.util.*;


/**
 * RETOUR TP1
 * PAS DE VERIF SUR LES PARAMS sur addline
 */

/**
 * Class representing a station.
 */
public class Station {

    String name;
    Set<String> lines;
    Map<String, Integer> numbers;
    Map<String, Double> distances;

    /** Builds a station associated to no lines.
     * @param _name the name of the station.
     */
    public Station(String _name) {
        name = _name;
        this.lines = new HashSet<>();
        this.numbers = new HashMap<>();
        this.distances = new HashMap<>();
    }

    /**
     * Builds a station, initially associated to a given line with a given number.
     * @param _name the name of the station
     * @param _line the name of the line associated to the station
     * @param _number the number of the station on the considered line
     * @param _dist the distance of the station on the considered line
     */
    public Station(String _name, String _line, int _number, double _dist) {
        name = _name;
        this.lines = new HashSet<>();
        this.numbers = new HashMap<>();
        this.distances = new HashMap<>();
        this.addLine(_line, _number, _dist);
    }


    /**
     * Adds a line to the current station, with the appropriate parameters.
     * If the line already exists, the previous information are overwritten.
     * @param _line the name of the line associated to the station
     * @param _number the number of the station on the considered line
     * @param _dist the distance of the station on the considered line
     */
    public void addLine(String _line, int _number, double _dist) {
        if(this.lines.contains(_line)){
            this.numbers.replace(_line, _number);
            this.distances.replace(_line, _dist);
        } else {
            this.lines.add(_line);
            this.numbers.put(_line, _number);
            this.distances.put(_line, _dist);
        }
    }


    /**
     * Removes a line from the station.
     * @param _line the line to remove.
     */
    public void removeLine(String _line) {
        if(this.lines.contains(_line)){
            this.lines.remove(_line);
            this.numbers.remove(_line);
            this.distances.remove(_line);
        }
    }


    /**
     * Retrieves the name of the station.
     * @return the name of the station
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the number of the station on a given line.
     * @param l The name of the line
     * @return the # of the station for the given line,
     *         or 0 if the line does not exist at the station.
     */
    public int getNumberForLine(String l) {
        if(this.lines.contains(l)){
            return this.numbers.get(l);
        } else {
            return 0;
        }
    }


    /**
     * Returns the distance of the station on a given line.
     * @param l The name of the line.
     * @return the distance of the station w.r.t. the beginning of the line.
     */
    public double getDistanceForLine(String l) {
        if(this.lines.contains(l)){
            return this.distances.get(l);
        } else {
            return -1.0;
        }
    }

    /**
     * Computes the set of lines associated to the station.
     * @return a set containing the names of the lines that cross the station.
     */
    public Set<String> getLines() {
        return this.lines;
    }


    @Override
    public int hashCode() {
        return Objects.hash(this.name);
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Station){
            Station st=(Station)o;
            if(this.name.equals(st.name)){
                return true;
            }
        }
        return false;
    }
}
