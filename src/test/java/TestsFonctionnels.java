import fr.ufc.l3info.oprog.Barrier;
import fr.ufc.l3info.oprog.ITicket;
import fr.ufc.l3info.oprog.Network;
import fr.ufc.l3info.oprog.TicketMachine;
import fr.ufc.l3info.oprog.parser.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;


public class TestsFonctionnels {

    /** Instance singleton du parser de stations */
    final NetworkParser parser = NetworkParser.getInstance();

    @Test
    public void testFonctionnelsValideAdulte() throws IOException, NetworkParserException {
        //CREER LE RESEAU À PARTIR D'UN FICHIER TEXTE (metroOK.txt)
        ASTNode n = parser.parse(new File("./target/classes/data/metroOK.txt"));
        NetworkBuilder builder = new NetworkBuilder();
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        n.accept(v);
        n.accept(builder);
        Network net = builder.getNetwork();
        //Le client utilise la TicketMachine pour instancier son ticket
        TicketMachine tm=TicketMachine.getInstance();
        ITicket t =tm.buyTicket(false,10);
        //Le ticket est passé dans une barrière d'entrée, situé dans une certaine station du réseau.
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        p.put(4.15,15);
        p.put(8.3,20);
        Barrier b0 = Barrier.build(net, "s0", p);
        b0.enter(t);
        //Le client ressort à une autre station, en empruntant la barrière de sortie.
        Barrier b1 = Barrier.build(net, "s1", p);
        Assert.assertTrue(b1.exit(t));
    }
    @Test
    public void testFonctionnelsValideChild() throws IOException, NetworkParserException {
        //CREER LE RESEAU À PARTIR D'UN FICHIER TEXTE (metroOK.txt)
        ASTNode n = parser.parse(new File("./target/classes/data/metroOK.txt"));
        NetworkBuilder builder = new NetworkBuilder();
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        n.accept(v);
        n.accept(builder);
        Network net = builder.getNetwork();
        //Le client utilise la TicketMachine pour instancier son ticket
        TicketMachine tm=TicketMachine.getInstance();
        ITicket t =tm.buyTicket(true,7);
        //Le ticket est passé dans une barrière d'entrée, situé dans une certaine station du réseau.
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        p.put(4.15,15);
        p.put(8.3,20);
        Barrier b0 = Barrier.build(net, "s0", p);
        b0.enter(t);
        //Le client ressort à une autre station, en empruntant la barrière de sortie.
        Barrier b1 = Barrier.build(net, "s1", p);
        Assert.assertTrue(b1.exit(t));
    }
    @Test
    public void testFonctionnelsNonValideAdulte() throws IOException, NetworkParserException {
        //CREER LE RESEAU À PARTIR D'UN FICHIER TEXTE (metroOK.txt)
        ASTNode n = parser.parse(new File("./target/classes/data/metroOK.txt"));
        NetworkBuilder builder = new NetworkBuilder();
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        n.accept(v);
        n.accept(builder);
        Network net = builder.getNetwork();
        //Le client utilise la TicketMachine pour instancier son ticket
        TicketMachine tm=TicketMachine.getInstance();
        ITicket t =tm.buyTicket(false,9);
        //Le ticket est passé dans une barrière d'entrée, situé dans une certaine station du réseau.
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        p.put(4.15,15);
        p.put(8.3,20);
        Barrier b0 = Barrier.build(net, "s0", p);
        b0.enter(t);
        //Le client ressort à une autre station, en empruntant la barrière de sortie.
        Barrier b1 = Barrier.build(net, "s1", p);
        Assert.assertFalse(b1.exit(t));
    }
    @Test
    public void testFonctionnelsNonValideChild() throws IOException, NetworkParserException {
        //CREER LE RESEAU À PARTIR D'UN FICHIER TEXTE (metroOK.txt)
        ASTNode n = parser.parse(new File("./target/classes/data/metroOK.txt"));
        NetworkBuilder builder = new NetworkBuilder();
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        n.accept(v);
        n.accept(builder);
        Network net = builder.getNetwork();
        //Le client utilise la TicketMachine pour instancier son ticket
        TicketMachine tm=TicketMachine.getInstance();
        ITicket t =tm.buyTicket(true,3);
        //Le ticket est passé dans une barrière d'entrée, situé dans une certaine station du réseau.
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        p.put(4.15,15);
        p.put(8.3,20);
        Barrier b0 = Barrier.build(net, "s0", p);
        b0.enter(t);
        //Le client ressort à une autre station, en empruntant la barrière de sortie.
        Barrier b1 = Barrier.build(net, "s1", p);
        Assert.assertFalse(b1.exit(t));
    }
    @Test
    public void testFonctionnelsValideReajustedAdulte() throws IOException, NetworkParserException {
        //CREER LE RESEAU À PARTIR D'UN FICHIER TEXTE (metroOK.txt)
        ASTNode n = parser.parse(new File("./target/classes/data/metroOK.txt"));
        NetworkBuilder builder = new NetworkBuilder();
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        n.accept(v);
        n.accept(builder);
        Network net = builder.getNetwork();
        //Le client utilise la TicketMachine pour instancier son ticket
        TicketMachine tm=TicketMachine.getInstance();
        ITicket t =tm.buyTicket(false,9);
        //Le ticket est passé dans une barrière d'entrée, situé dans une certaine station du réseau.
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        p.put(4.15,15);
        p.put(8.3,20);
        Barrier b0 = Barrier.build(net, "s0", p);
        b0.enter(t);
        //Le client ressort à une autre station, en empruntant la barrière de sortie.
        Barrier b1 = Barrier.build(net, "s1", p);
        Assert.assertFalse(b1.exit(t));
        //Pas assez de credit sur le ticket on le reajuste
        t = tm.adjustFare(t,1);
        Assert.assertTrue(b1.exit(t));
    }
    @Test
    public void testFonctionnelsValideReajustedChild() throws IOException, NetworkParserException {
        //CREER LE RESEAU À PARTIR D'UN FICHIER TEXTE (metroOK.txt)
        ASTNode n = parser.parse(new File("./target/classes/data/metroOK.txt"));
        NetworkBuilder builder = new NetworkBuilder();
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        n.accept(v);
        n.accept(builder);
        Network net = builder.getNetwork();
        //Le client utilise la TicketMachine pour instancier son ticket
        TicketMachine tm=TicketMachine.getInstance();
        ITicket t =tm.buyTicket(true,3);
        //Le ticket est passé dans une barrière d'entrée, situé dans une certaine station du réseau.
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        p.put(4.15,15);
        p.put(8.3,20);
        Barrier b0 = Barrier.build(net, "s0", p);
        b0.enter(t);
        //Le client ressort à une autre station, en empruntant la barrière de sortie.
        Barrier b1 = Barrier.build(net, "s1", p);
        Assert.assertFalse(b1.exit(t));
        //Pas assez de credit sur le ticket on le reajuste
        t = tm.adjustFare(t,2);
        Assert.assertTrue(b1.exit(t));
    }
    @Test
    public void testFonctionnelsValideTenIllimitedTicketAdulte() throws IOException, NetworkParserException {
        //CREER LE RESEAU À PARTIR D'UN FICHIER TEXTE (metroOK.txt)
        ASTNode n = parser.parse(new File("./target/classes/data/metroOK.txt"));
        NetworkBuilder builder = new NetworkBuilder();
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        n.accept(v);
        n.accept(builder);
        Network net = builder.getNetwork();
        //Le client utilise la TicketMachine pour instancier son ticket cela 10 fois
        TicketMachine tm=TicketMachine.getInstance();
        ITicket t = tm.buyTicket(false);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        p.put(4.15,15);
        p.put(8.3,20);
        Barrier b0 = Barrier.build(net, "s0", p);
        Barrier b1 = Barrier.build(net, "s1", p);
        for(int i=0;i<10;i++){
            Assert.assertTrue(b0.enter(t));
        }
    }

    @Test
    public void testFonctionnelsNonValideTenIllimitedTicketAdulte() throws IOException, NetworkParserException {
        //CREER LE RESEAU À PARTIR D'UN FICHIER TEXTE (metroOK.txt)
        ASTNode n = parser.parse(new File("./target/classes/data/metroOK.txt"));
        NetworkBuilder builder = new NetworkBuilder();
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        n.accept(v);
        n.accept(builder);
        Network net = builder.getNetwork();
        //Le client utilise la TicketMachine pour instancier son ticket cela 10 fois
        TicketMachine tm=TicketMachine.getInstance();
        ITicket t = tm.buyTicket(false);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        p.put(4.15,15);
        p.put(8.3,20);
        Barrier b0 = Barrier.build(net, "s0", p);
        Barrier b1 = Barrier.build(net, "s1", p);
        for(int i=0;i<10;i++){
            Assert.assertTrue(b0.enter(t));
        }
        Assert.assertFalse(b0.enter(t));
    }

    @Test
    public void testFonctionnelsValideTenIllimitedTicketAdulteWithExit() throws IOException, NetworkParserException {
        //CREER LE RESEAU À PARTIR D'UN FICHIER TEXTE (metroOK.txt)
        ASTNode n = parser.parse(new File("./target/classes/data/metroOK.txt"));
        NetworkBuilder builder = new NetworkBuilder();
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        n.accept(v);
        n.accept(builder);
        Network net = builder.getNetwork();
        //Le client utilise la TicketMachine pour instancier son ticket cela 10 fois
        TicketMachine tm=TicketMachine.getInstance();
        ITicket t = tm.buyTicket(false);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        p.put(4.15,15);
        p.put(8.3,20);
        Barrier b0 = Barrier.build(net, "s0", p);
        Barrier b1 = Barrier.build(net, "s1", p);
        for(int i=0;i<10;i++){
            Assert.assertTrue(b0.enter(t));
            Assert.assertTrue(b1.exit(t));
        }
    }

    @Test
    public void testFonctionnelsNonValideTenIllimitedTicketAdulteWithExit() throws IOException, NetworkParserException {
        //CREER LE RESEAU À PARTIR D'UN FICHIER TEXTE (metroOK.txt)
        ASTNode n = parser.parse(new File("./target/classes/data/metroOK.txt"));
        NetworkBuilder builder = new NetworkBuilder();
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        n.accept(v);
        n.accept(builder);
        Network net = builder.getNetwork();
        //Le client utilise la TicketMachine pour instancier son ticket cela 10 fois
        TicketMachine tm=TicketMachine.getInstance();
        ITicket t = tm.buyTicket(false);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        p.put(4.15,15);
        p.put(8.3,20);
        Barrier b0 = Barrier.build(net, "s0", p);
        Barrier b1 = Barrier.build(net, "s1", p);
        for(int i=0;i<10;i++){
            Assert.assertTrue(b0.enter(t));
            Assert.assertTrue(b1.exit(t));
        }
        Assert.assertFalse(b0.enter(t));
        Assert.assertFalse(b1.exit(t));
    }

    @Test
    public void testFonctionnelsValideTenIllimitedTicketChild() throws IOException, NetworkParserException {
        //CREER LE RESEAU À PARTIR D'UN FICHIER TEXTE (metroOK.txt)
        ASTNode n = parser.parse(new File("./target/classes/data/metroOK.txt"));
        NetworkBuilder builder = new NetworkBuilder();
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        n.accept(v);
        n.accept(builder);
        Network net = builder.getNetwork();
        //Le client utilise la TicketMachine pour instancier son ticket cela 10 fois
        TicketMachine tm=TicketMachine.getInstance();
        ITicket t = tm.buyTicket(true);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        p.put(4.15,15);
        p.put(8.3,20);
        Barrier b0 = Barrier.build(net, "s0", p);
        Barrier b1 = Barrier.build(net, "s1", p);
        for(int i=0;i<10;i++){
            Assert.assertTrue(b0.enter(t));
        }
    }

    @Test
    public void testFonctionnelsNonValideTenIllimitedTicketChild() throws IOException, NetworkParserException {
        //CREER LE RESEAU À PARTIR D'UN FICHIER TEXTE (metroOK.txt)
        ASTNode n = parser.parse(new File("./target/classes/data/metroOK.txt"));
        NetworkBuilder builder = new NetworkBuilder();
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        n.accept(v);
        n.accept(builder);
        Network net = builder.getNetwork();
        //Le client utilise la TicketMachine pour instancier son ticket cela 10 fois
        TicketMachine tm=TicketMachine.getInstance();
        ITicket t = tm.buyTicket(true);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        p.put(4.15,15);
        p.put(8.3,20);
        Barrier b0 = Barrier.build(net, "s0", p);
        Barrier b1 = Barrier.build(net, "s1", p);
        for(int i=0;i<10;i++){
            Assert.assertTrue(b0.enter(t));
        }
        Assert.assertFalse(b0.enter(t));
    }

    @Test
    public void testFonctionnelsValideTenIllimitedTicketChildWithExit() throws IOException, NetworkParserException {
        //CREER LE RESEAU À PARTIR D'UN FICHIER TEXTE (metroOK.txt)
        ASTNode n = parser.parse(new File("./target/classes/data/metroOK.txt"));
        NetworkBuilder builder = new NetworkBuilder();
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        n.accept(v);
        n.accept(builder);
        Network net = builder.getNetwork();
        //Le client utilise la TicketMachine pour instancier son ticket cela 10 fois
        TicketMachine tm=TicketMachine.getInstance();
        ITicket t = tm.buyTicket(true);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        p.put(4.15,15);
        p.put(8.3,20);
        Barrier b0 = Barrier.build(net, "s0", p);
        Barrier b1 = Barrier.build(net, "s1", p);
        for(int i=0;i<10;i++){
            Assert.assertTrue(b0.enter(t));
            Assert.assertTrue(b1.exit(t));
        }
    }

    @Test
    public void testFonctionnelsNonValideTenIllimitedTicketChildWithExit() throws IOException, NetworkParserException {
        //CREER LE RESEAU À PARTIR D'UN FICHIER TEXTE (metroOK.txt)
        ASTNode n = parser.parse(new File("./target/classes/data/metroOK.txt"));
        NetworkBuilder builder = new NetworkBuilder();
        ASTCheckerVisitor v = new ASTCheckerVisitor();
        n.accept(v);
        n.accept(builder);
        Network net = builder.getNetwork();
        //Le client utilise la TicketMachine pour instancier son ticket cela 10 fois
        TicketMachine tm=TicketMachine.getInstance();
        ITicket t = tm.buyTicket(true);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        p.put(4.15,15);
        p.put(8.3,20);
        Barrier b0 = Barrier.build(net, "s0", p);
        Barrier b1 = Barrier.build(net, "s1", p);
        for(int i=0;i<10;i++){
            Assert.assertTrue(b0.enter(t));
            Assert.assertTrue(b1.exit(t));
        }
        Assert.assertFalse(b0.enter(t));
        Assert.assertFalse(b1.exit(t));
    }
}
