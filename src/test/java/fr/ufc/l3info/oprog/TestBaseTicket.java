package fr.ufc.l3info.oprog;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class TestBaseTicket {
    //Test isChild
    @Test
    public void isChild_IsChild(){
        ITicket t = new BaseTicket(true,0);
        Assert.assertTrue(t.isChild());
    }
    @Test
    public void isChild_IsNotChild(){
        ITicket t = new BaseTicket(false,0);
        Assert.assertFalse(t.isChild());
    }

    //Test getAmount à rajouter selon l'état du ticket
    @Test
    public void getAmount_Zero(){
        ITicket t = new BaseTicket(true,0);
        Assert.assertEquals(t.getAmount(),0);
    }
    @Test
    public void getAmount_NegativeValueConstructor(){
        ITicket t = new BaseTicket(true,-1);
        Assert.assertEquals(t.getAmount(),0);
    }
    @Test
    public void getAmount_SupZero(){
        ITicket t = new BaseTicket(true,12);
        Assert.assertEquals(t.getAmount(),12);
    }

    //Test entering
    @Test
    public void entering_issued_ParamValid(){
        ITicket t = new BaseTicket(true,12);
        Assert.assertTrue(t.entering("Sanatorium"));
        Assert.assertTrue(t.isValid() && t.getEntryStation()=="Sanatorium");
    }
    @Test
    public void entering_issued_StringNull(){
        ITicket t = new BaseTicket(true,12);
        Assert.assertFalse(t.entering(null));
        Assert.assertFalse(t.isValid());
    }
    @Test
    public void entering_issued_StringVoid_KO(){
        ITicket t = new BaseTicket(true,12);
        Assert.assertFalse(t.entering("")); // Erreur dans l'implémentation 07 resultat attendu FALSE
        Assert.assertFalse(t.isValid());
    }
    @Test
    public void entering_entered(){
        ITicket t = new BaseTicket(true,12);
        t.entering("Sanatorium");
        Assert.assertFalse(t.entering("UFR Sciences"));
        Assert.assertFalse(t.isValid());
    }
    @Test
    public void entering_invalid(){
        ITicket t = new BaseTicket(true,12);
        t.invalidate();
        Assert.assertFalse(t.entering("UFR Sciences"));
        Assert.assertFalse(t.isValid());
    }

    //Test invalidate
    @Test
    public void invalidate_Issued(){
        ITicket t = new BaseTicket(true,12);
        t.invalidate();
        Assert.assertFalse(t.isValid());
    }
    @Test
    public void invalidate_Entered(){
        ITicket t = new BaseTicket(true,12);
        t.entering("Sanatorium");
        t.invalidate();
        Assert.assertFalse(t.isValid());
    }
    @Test
    public void invalidate_Invalid(){
        ITicket t = new BaseTicket(true,12);
        t.invalidate();
        t.invalidate();
        Assert.assertFalse(t.isValid());
    }

    //Test getEntryStation
    @Test
    public void getEntryStation_Issued(){
        ITicket t = new BaseTicket(true,12);
        Assert.assertNull(t.getEntryStation());
    }
    @Test
    public void getEntryStation_Entered(){
        ITicket t = new BaseTicket(true,12);
        t.entering("Sanatorium");
        Assert.assertEquals(t.getEntryStation(),"Sanatorium");
    }
    @Test
    public void getEntryStation_Invalid(){
        ITicket t = new BaseTicket(true,12);
        t.invalidate();
        Assert.assertNull(t.getEntryStation());
    }
    @Test
    public void getEntryStation_IssuedThenEnteredThenInvalid(){
        ITicket t = new BaseTicket(true,12);
        t.entering("Sanatorium");
        t.entering("Sanatorium");
        Assert.assertNull(t.getEntryStation());
    }

    //Test isValid
    @Test
    public void isValid_Issued(){
        ITicket t = new BaseTicket(true,12);
        Assert.assertTrue(t.isValid());
    }
    @Test
    public void isValid_Entered(){
        ITicket t = new BaseTicket(true,12);
        t.entering("Sanatorium");
        Assert.assertTrue(t.isValid());
    }
    @Test
    public void isValid_Invalid(){
        ITicket t = new BaseTicket(true,12);
        t.invalidate();
        Assert.assertFalse(t.isValid());
    }
    @Test
    public void isValid_IssuedThenEnteredThenInvalid(){
        ITicket t = new BaseTicket(true,12);
        t.entering("Sanatorium");
        t.entering("Sanatorium");
        Assert.assertFalse(t.isValid());
    }
}
