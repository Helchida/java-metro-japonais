package fr.ufc.l3info.oprog;

import org.junit.Assert;
import org.junit.Test;

public class TestTicketMachine {
    @Test
    public void BuyTicketMachineSimpleAdulte(){
        TicketMachine tm=TicketMachine.getInstance();
        ITicket b=tm.buyTicket(false,10);
        Assert.assertEquals(b.getAmount(),10);
        Assert.assertFalse(b.isChild());
    }
    @Test
    public void BuyTicketMachineSimpleEnfant(){
        TicketMachine tm=TicketMachine.getInstance();
        ITicket b=tm.buyTicket(true,10);
        Assert.assertEquals(b.getAmount(),10);
        Assert.assertTrue(b.isChild());
    }

    @Test
    public void BuyTicketMachineTenTripsAdulte(){
        TicketMachine tm=TicketMachine.getInstance();
        ITicket b=tm.buyTicket(false);
        Assert.assertEquals(b.getAmount(),Integer.MAX_VALUE);
        Assert.assertFalse(b.isChild());
    }

    @Test
    public void BuyTicketMachineTenTripsEnfant(){
        TicketMachine tm=TicketMachine.getInstance();
        ITicket b=tm.buyTicket(true);
        Assert.assertEquals(b.getAmount(),Integer.MAX_VALUE);
        Assert.assertTrue(b.isChild());
    }

    @Test
    public void BuyTicketMachineNegativeAmountAdulte(){
        TicketMachine tm=TicketMachine.getInstance();
        ITicket b=tm.buyTicket(false, -5);
        Assert.assertNull(b);
    }

    @Test
    public void BuyTicketMachineNegativeAmountEnfant(){
        TicketMachine tm=TicketMachine.getInstance();
        ITicket b=tm.buyTicket(true, -5);
        Assert.assertNull(b);
    }

    @Test
    public void AdjustFareSimpleAdulte(){
        TicketMachine tm=TicketMachine.getInstance();
        ITicket old_t=tm.buyTicket(false,10);
        ITicket new_t = tm.adjustFare(old_t,5);
        Assert.assertEquals(new_t.getAmount(),15);
        Assert.assertFalse(new_t.isChild());
    }
    @Test
    public void AdjustFareSimpleEnfant(){
        TicketMachine tm=TicketMachine.getInstance();
        ITicket old_t=tm.buyTicket(true,10);
        ITicket new_t = tm.adjustFare(old_t,5);
        Assert.assertEquals(new_t.getAmount(),15);
        Assert.assertTrue(new_t.isChild());
    }

    @Test
    public void AdjustFareTenTripsAdulte(){
        TicketMachine tm=TicketMachine.getInstance();
        ITicket old_t=tm.buyTicket(false);
        ITicket new_t = tm.adjustFare(old_t,5);
        Assert.assertEquals(new_t.getAmount(),Integer.MAX_VALUE);
        Assert.assertFalse(new_t.isChild());
    }

    @Test
    public void AdjustFareTenTripsEnfant(){
        TicketMachine tm=TicketMachine.getInstance();
        ITicket old_t=tm.buyTicket(true);
        ITicket new_t = tm.adjustFare(old_t,5);
        Assert.assertEquals(new_t.getAmount(),Integer.MAX_VALUE);
        Assert.assertTrue(new_t.isChild());
    }

    @Test
    public void AdjustFareNegativeAmountAdulte(){
        TicketMachine tm=TicketMachine.getInstance();
        ITicket old_t=tm.buyTicket(false, 10);
        ITicket new_t = tm.adjustFare(old_t,-5);
        Assert.assertEquals(new_t.getAmount(),10);
        Assert.assertFalse(new_t.isChild());
    }

    @Test
    public void AdjustFareNegativeAmountEnfant(){
        TicketMachine tm=TicketMachine.getInstance();
        ITicket old_t=tm.buyTicket(true, 10);
        ITicket new_t = tm.adjustFare(old_t,-5);
        Assert.assertEquals(new_t.getAmount(),10);
        Assert.assertTrue(new_t.isChild());
    }

    @Test
    public void AdjustFareNullTicketAdulte(){
        TicketMachine tm=TicketMachine.getInstance();
        ITicket old_t=tm.buyTicket(false, -5);
        ITicket new_t = tm.adjustFare(old_t,15);
        Assert.assertNull(new_t);
    }

    @Test
    public void AdjustFareNullTicketEnfant(){
        TicketMachine tm=TicketMachine.getInstance();
        ITicket old_t=tm.buyTicket(true, -5);
        ITicket new_t = tm.adjustFare(old_t,15);
        Assert.assertNull(new_t);
    }

}
