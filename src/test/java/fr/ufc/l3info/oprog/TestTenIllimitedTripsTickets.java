package fr.ufc.l3info.oprog;

import org.junit.Assert;
import org.junit.Test;

public class TestTenIllimitedTripsTickets {

    @Test
    public void CreationIllimitedTripsTicketNotChild(){
        TenIllimitedTripsTicket ti = new TenIllimitedTripsTicket(false);
        Assert.assertFalse(ti.isChild());
        Assert.assertTrue(ti.isValid());
    }

    @Test
    public void CreationIllimitedTripsTicketIsChild(){
        TenIllimitedTripsTicket ti = new TenIllimitedTripsTicket(true);
        Assert.assertTrue(ti.isChild());
        Assert.assertTrue(ti.isValid());
    }

    @Test
    public void CreationIllimitedTripsTicketEnteringValid(){
        TenIllimitedTripsTicket ti = new TenIllimitedTripsTicket(false);
        Assert.assertFalse(ti.isChild());

        Assert.assertTrue(ti.entering("Olympique de Marseille"));
        Assert.assertTrue(ti.isValid());
    }


    @Test
    public void CreationIllimitedTripsTicketEntering10Entry(){
        TenIllimitedTripsTicket ti = new TenIllimitedTripsTicket(false);
        Assert.assertFalse(ti.isChild());

        Assert.assertTrue(ti.entering("Olympique de Marseille"));
        Assert.assertTrue(ti.entering("Paris Saint Germain"));
        Assert.assertTrue(ti.entering("Olympique Lyonnais"));
        Assert.assertTrue(ti.entering("Monaco"));
        Assert.assertTrue(ti.entering("Sochaux"));
        Assert.assertTrue(ti.entering("Auxerre"));
        Assert.assertTrue(ti.entering("Dijon"));
        Assert.assertTrue(ti.entering("Metz"));
        Assert.assertTrue(ti.entering("ASSE"));
        Assert.assertTrue(ti.entering("LOSC"));


        Assert.assertTrue(ti.isValid());
    }

    @Test
    public void CreationIllimitedTripsTicketEntering11Entry(){
        TenIllimitedTripsTicket ti = new TenIllimitedTripsTicket(false);
        Assert.assertFalse(ti.isChild());

        Assert.assertTrue(ti.entering("Olympique de Marseille"));
        Assert.assertTrue(ti.entering("Paris Saint Germain"));
        Assert.assertTrue(ti.entering("Olympique Lyonnais"));
        Assert.assertTrue(ti.entering("Monaco"));
        Assert.assertTrue(ti.entering("Sochaux"));
        Assert.assertTrue(ti.entering("Auxerre"));
        Assert.assertTrue(ti.entering("Dijon"));
        Assert.assertTrue(ti.entering("Metz"));
        Assert.assertTrue(ti.entering("ASSE"));
        Assert.assertTrue(ti.entering("LOSC"));
        Assert.assertFalse(ti.entering("NICE"));


        Assert.assertFalse(ti.isValid());
    }


    @Test
    public void CreationIllimitedTripsTicketEntryStationValid(){
        TenIllimitedTripsTicket ti = new TenIllimitedTripsTicket(false);
        Assert.assertFalse(ti.isChild());

        Assert.assertTrue(ti.entering("Olympique de Marseille"));

        Assert.assertEquals(ti.getEntryStation(),"Olympique de Marseille");
        Assert.assertTrue(ti.isValid());
    }

    @Test
    public void CreationIllimitedTripsTicketEntryStationNoEntering(){
        TenIllimitedTripsTicket ti = new TenIllimitedTripsTicket(false);
        Assert.assertFalse(ti.isChild());


        Assert.assertNull(ti.getEntryStation());
        Assert.assertTrue(ti.isValid());
    }

    @Test
    public void CreationIllimitedTripsTicketGetAmount(){
        TenIllimitedTripsTicket ti = new TenIllimitedTripsTicket(false);
        Assert.assertFalse(ti.isChild());

        Assert.assertEquals(ti.getAmount(),Integer.MAX_VALUE);

        Assert.assertTrue(ti.isValid());
    }
}
