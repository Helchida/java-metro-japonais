package fr.ufc.l3info.oprog;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class TestNetwork {

    //Test constructeur
    @Test
    public void constructor(){
        Network n = new Network();
    }

    //Test addStation
    @Test
    public void addStation_NotYetIn(){
        Network n = new Network();
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Assert.assertEquals(s,n.getStationByName("Edison"));
    }
    @Test
    public void addStation_AlreadyIn(){
        Network n = new Network();
        Station s1 = new Station("Edison","L10",2,3.7);
        Station s2 = new Station("Edison","L10",2,3.7);
        n.addStation(s1);
        Assert.assertSame(n.getStationByName("Edison"), s1);
        Assert.assertNotSame(n.getStationByName("Edison"), s2);
    }

    //Test getLines
    @Test
    public void getLines_OneLine(){
        Station s = new Station("Edison","L10",2,3.7);
        Network n = new Network();
        n.addStation(s);
        Assert.assertTrue(n.getLines().contains("L10"));
        Assert.assertEquals(1, n.getLines().size());
    }
    @Test
    public void getLines_ManyLines(){
        Network n = new Network();
        Station s1 = new Station("Edison","L10",2,3.7);
        Station s2 = new Station("Sanatorium","L9",3,4.7);
        s1.addLine("L9",5,5.9);
        n.addStation(s1);
        n.addStation(s2);
        Assert.assertTrue(n.getLines().contains("L10"));
        Assert.assertTrue(n.getLines().contains("L9"));
        Assert.assertEquals(2, n.getLines().size());
    }

    //Test getStationByName
    @Test
    public void getStationByName_Exist(){
        Network n = new Network();
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Assert.assertEquals(n.getStationByName("Edison"),s);
    }
    @Test
    public void getStationByName_DoesNotExist(){
        Network n = new Network();
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Assert.assertNull(n.getStationByName("Sanatorium"));
    }


    //Test getStationByLineAndNumber
    @Test
    public void getStationByLineAndNumber_Exist(){
        Network n = new Network();
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Assert.assertEquals(n.getStationByLineAndNumber("L10",2),s);
    }
    @Test
    public void getStationByLineAndNumber_OnlyLine(){
        Network n = new Network();
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Assert.assertNull(n.getStationByLineAndNumber("L10",4));
    }
    @Test
    public void getStationByLineAndNumber_OnlyNumber(){
        Network n = new Network();
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Assert.assertNull(n.getStationByLineAndNumber("L9",2));
    }
    @Test
    public void getStationByLineAndNumber_DoesNotExist(){
        Network n = new Network();
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Assert.assertNull(n.getStationByLineAndNumber("L9",3));
    }

    //Test isValid
    @Test
    public void isValid_valid(){
        Network n = new Network();
        Station s = new Station("Edison","L10",1,0);
        n.addStation(s);
        Assert.assertTrue(n.isValid());
    }
    @Test public void isValid_NetworkVoid(){
        Network n = new Network();
        Assert.assertFalse(n.isValid());
    }
    @Test public void isValid_ZeroLine(){
        Network n = new Network();
        Station s = new Station("Edison");
        n.addStation(s);
        Assert.assertFalse(n.isValid());
    }
    @Test public void isValid_LineNotValid_FirstStation_NumberIsNotZero(){
        Network n = new Network();
        Station s = new Station("Edison","L10",4,0);
        n.addStation(s);
        Assert.assertFalse(n.isValid());
    }
    @Test public void isValid_LineNotValid_FirstStation_KilometreIsNotZero(){
        Network n = new Network();
        Station s = new Station("Edison","L10",0,1);
        n.addStation(s);
        Assert.assertFalse(n.isValid());
    }
    @Test public void isValid_LineNotValid_NumberMissing(){
        Network n = new Network();
        Station s1 = new Station("Edison","L9",0,0);
        Station s2 = new Station("Sanatorium","L9",1,1.1);
        Station s3 = new Station("Beaux Art","L9",2,2.2);
        Station s4 = new Station("UFR Sciences","L9",5,5.5);
        Station s5 = new Station("CHRU Minjoz","L9",6,6.6);
        n.addStation(s1);
        n.addStation(s2);
        n.addStation(s3);
        n.addStation(s4);
        n.addStation(s5);
        Assert.assertFalse(n.isValid());
    }
    @Test public void isValid_LineNotValid_NumberNotUnique(){
        Network n = new Network();
        Station s1 = new Station("Edison","L9",0,0);
        Station s2 = new Station("Sanatorium","L9",1,1.1);
        Station s3 = new Station("Beaux Art","L9",2,2.2);
        Station s4 = new Station("UFR Sciences","L9",2,5.5);
        Station s5 = new Station("CHRU Minjoz","L9",3,6.6);
        n.addStation(s1);
        n.addStation(s2);
        n.addStation(s3);
        n.addStation(s4);
        n.addStation(s5);
        Assert.assertFalse(n.isValid());
    }
    @Test public void isValid_LineNotValid_DistanceIsNotGrowingWithNumber(){
        Network n = new Network();
        Station s1 = new Station("Edison","L9",0,0);
        Station s2 = new Station("Sanatorium","L9",1,1.1);
        Station s3 = new Station("Beaux Art","L9",2,2.2);
        Station s4 = new Station("UFR Sciences","L9",3,0.3);
        Station s5 = new Station("CHRU Minjoz","L9",4,4.4);
        n.addStation(s1);
        n.addStation(s2);
        n.addStation(s3);
        n.addStation(s4);
        n.addStation(s5);
        Assert.assertFalse(n.isValid());
    }
    @Test public void isValid_lineIsolated(){
        Network n = new Network();
        Station s1 = new Station("Edison","L9",1,0);
        Station s2 = new Station("Sanatorium","L9",2,1.1);
        Station s3 = new Station("Beaux Art","L10",1,0);
        Station s4 = new Station("UFR Sciences","L10",2,5.5);
        Station s5 = new Station("CHRU Minjoz","L10",3,6.6);
        n.addStation(s1);
        n.addStation(s2);
        n.addStation(s3);
        n.addStation(s4);
        n.addStation(s5);
        Assert.assertFalse(n.isValid());
    }

    //Test distance
    @Test
    public void distance_Simple(){
        Network n = new Network();
        Station s1 = new Station("Edison","L9",0,0);
        Station s2 = new Station("Sanatorium","L9",1,1.1);
        n.addStation(s1);
        n.addStation(s2);
        Assert.assertEquals(n.distance("Edison","Sanatorium"),1.1,0.001);
    }
    /*
    @Test
    public void distance_Invalid(){
        Network n = new Network();
        Station s1 = new Station("Edison","L9",0,0);
        Station s2 = new Station("Sanatorium","L9",1,1.1);
        n.addStation(s1);
        n.addStation(s2);
        Assert.assertTrue(n.distance("Edison","UFR Sciences")<0);
        Assert.assertTrue(n.distance("Edison",null)<0);
        Station s3 = new Station("Beaux Art","L10",0,0);
        n.addStation(s3);
        Assert.assertTrue(n.distance("Edison","Sanatorium")<0);
    }
    */

}
