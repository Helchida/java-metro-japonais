package fr.ufc.l3info.oprog;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TestBarrierIntegration {
    //Test de la fonction Build
    @Test
    public void testBuild_Simple(){
        Network n = new Network();
        String s1 = "Edison";
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        Barrier b = Barrier.build(n,s1,p);
        Assert.assertNotNull(b);
    }
    @Test
    public void testBuild_NetworkNull(){
        Network n = null;
        String s1 = "Edison";
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        Barrier b = Barrier.build(n,s1,p);
        Assert.assertNull(b);
    }
    @Test
    public void testBuild_StationNull(){
        Network n = new Network();
        String s1 = null;
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        Barrier b = Barrier.build(n,s1,p);
        Assert.assertNull(b);
    }
    @Test
    public void testBuild_PricesNull(){
        Network n = new Network();
        String s1 = "Edison";
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Map<Double,Integer> p = null;
        Barrier b = Barrier.build(n,s1,p);
        Assert.assertNull(b);
    }
    @Test
    public void testBuild_NetworkHaveZeroLine(){
        Network n = new Network();
        String s1 = "Edison";
        Station s = new Station("Edison");
        n.addStation(s);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        Barrier b = Barrier.build(n,s1,p);
        Assert.assertNull(b);
    }
    @Test
    public void testBuild_StationDoesNotBelongToNetwork(){
        Network n = new Network();
        String s1 = "Sanatorium";
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        Barrier b = Barrier.build(n,s1,p);
        Assert.assertNull(b);
    }
    @Test
    public void testBuild_PricesEmpty(){
        Network n = new Network();
        String s1 = "Edison";
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        Barrier b = Barrier.build(n,s1,p);
        Assert.assertNull(b);
    }
    @Test
    public void testBuild_NoPriceAreaOfZero(){
        Network n = new Network();
        String s1 = "Edison";
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(1.6,10);
        Barrier b = Barrier.build(n,s1,p);
        Assert.assertNull(b);
    }
    @Test
    public void testBuild_PriceAreaDistanceNegative(){
        Network n = new Network();
        String s1 = "Edison";
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(-1.0,10);
        Barrier b = Barrier.build(n,s1,p);
        Assert.assertNull(b);
    }
    @Test
    public void testBuild_PriceAreaPriceNegative(){
        Network n = new Network();
        String s1 = "Edison";
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,-10);
        Barrier b = Barrier.build(n,s1,p);
        Assert.assertNull(b);
    }
    @Test
    public void testBuild_PriceAreaPriceZero(){
        Network n = new Network();
        String s1 = "Edison";
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,0);
        Barrier b = Barrier.build(n,s1,p);
        Assert.assertNull(b);
    }

    //TEST de la fonction enter()
    @Test
    public void testEnter_Simple(){
        Network n = new Network();
        String s1 = "Edison";
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        Barrier b = Barrier.build(n,s1,p);
        Assert.assertNotNull(b);
        ITicket t = new BaseTicket(false,10);
        Assert.assertTrue(b.enter((t)));
    }
    @Test
    public void testEnter_TicketUnvalid(){
        Network n = new Network();
        String s1 = "Edison";
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        Barrier b = Barrier.build(n,s1,p);
        Assert.assertNotNull(b);
        ITicket t = new BaseTicket(false,10);
        t.invalidate();
        Assert.assertFalse(b.enter((t)));
    }
    @Test
    public void testEnter_TicketEntered(){
        Network n = new Network();
        String s1 = "Edison";
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        Barrier b = Barrier.build(n,s1,p);
        Assert.assertNotNull(b);
        ITicket t = new BaseTicket(false,10);
        Assert.assertTrue(b.enter((t)));
        Assert.assertFalse(b.enter((t)));
    }
    @Test
    public void testEnter_TicketAmountZero(){
        Network n = new Network();
        String s1 = "Edison";
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        Barrier b = Barrier.build(n,s1,p);
        Assert.assertNotNull(b);
        ITicket t = new BaseTicket(false,0);
        Assert.assertFalse(b.enter((t)));
    }
    @Test
    public void testEnter_TicketAmountNegative(){
        Network n = new Network();
        String s1 = "Edison";
        Station s = new Station("Edison","L10",2,3.7);
        n.addStation(s);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        Barrier b = Barrier.build(n,s1,p);
        Assert.assertNotNull(b);
        ITicket t = new BaseTicket(false,-1);
        Assert.assertFalse(b.enter((t)));
    }

    //TEST EXIT
    @Test
    public void testExit_simple(){
        Network n = new Network();
        String s1 = "Edison";
        String s2 = "Beaux-Art";
        Station s = new Station("Edison","L10",1,0.0);
        Station sPrime = new Station("Beaux-Art","L10",2,0.5);
        n.addStation(s);
        n.addStation(sPrime);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        p.put(1.0,15);
        Barrier b = Barrier.build(n,s1,p);
        Barrier bExit = Barrier.build(n,s2,p);
        Assert.assertNotNull(b);
        ITicket t = new BaseTicket(false,12);
        Assert.assertTrue(b.enter((t)));
        Assert.assertTrue(bExit.exit(t));
        Assert.assertFalse(t.isValid());
    }
    @Test
    public void testExit_simpleChild(){
        Network n = new Network();
        String s1 = "Edison";
        String s2 = "Beaux-Art";
        Station s = new Station("Edison","L10",1,0.0);
        Station sPrime = new Station("Beaux-Art","L10",2,0.5);
        n.addStation(s);
        n.addStation(sPrime);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        p.put(1.0,15);
        Barrier b = Barrier.build(n,s1,p);
        Barrier bExit = Barrier.build(n,s2,p);
        Assert.assertNotNull(b);
        ITicket t = new BaseTicket(true,6);
        Assert.assertTrue(b.enter((t)));
        Assert.assertTrue(bExit.exit(t));
        Assert.assertFalse(t.isValid());
    }
    @Test
    public void testExit_NotEnoughAmount(){
        Network n = new Network();
        String s1 = "Edison";
        String s2 = "Beaux-Art";
        Station s = new Station("Edison","L10",1,0.0);
        Station sPrime = new Station("Beaux-Art","L10",2,0.5);
        n.addStation(s);
        n.addStation(sPrime);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        p.put(1.0,15);
        Barrier b = Barrier.build(n,s1,p);
        Barrier bExit = Barrier.build(n,s2,p);
        Assert.assertNotNull(b);
        ITicket t = new BaseTicket(false,8);
        Assert.assertTrue(b.enter((t)));
        Assert.assertFalse(bExit.exit(t));
        Assert.assertTrue(t.isValid());
    }
    @Test
    public void testExit_NotEnoughAmountChild(){
        Network n = new Network();
        String s1 = "Edison";
        String s2 = "Beaux-Art";
        Station s = new Station("Edison","L10",1,0.0);
        Station sPrime = new Station("Beaux-Art","L10",2,0.5);
        n.addStation(s);
        n.addStation(sPrime);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        p.put(1.0,15);
        Barrier b = Barrier.build(n,s1,p);
        Barrier bExit = Barrier.build(n,s2,p);
        Assert.assertNotNull(b);
        ITicket t = new BaseTicket(true,4);
        Assert.assertTrue(b.enter((t)));
        Assert.assertFalse(bExit.exit(t));
        Assert.assertTrue(t.isValid());
    }
    @Test
    public void testExit_NotEnteringYet(){
        Network n = new Network();
        String s1 = "Edison";
        String s2 = "Beaux-Art";
        Station s = new Station("Edison","L10",1,0.0);
        Station sPrime = new Station("Beaux-Art","L10",2,0.5);
        n.addStation(s);
        n.addStation(sPrime);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        p.put(1.0,15);
        Barrier b = Barrier.build(n,s1,p);
        Barrier bExit = Barrier.build(n,s2,p);
        Assert.assertNotNull(b);
        ITicket t = new BaseTicket(false,15);
        Assert.assertFalse(bExit.exit(t));
        Assert.assertTrue(t.isValid());
    }

}
