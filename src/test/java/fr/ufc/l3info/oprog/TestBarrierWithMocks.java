package fr.ufc.l3info.oprog;
// Mockito
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

// JUnit
import org.junit.runner.RunWith;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@RunWith(MockitoJUnitRunner.class)
public class TestBarrierWithMocks {

    //Test de la fonction Build
    @Test
    public void testBuild_Simple(){
        Network n = Mockito.mock(Network.class);
        String s = "Sanatorium";
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        Set<String> lines = new HashSet<String>();
        lines.add("L7");
        Mockito.when(n.getLines()).thenReturn(lines);
        Mockito.when(n.getStationByName("Sanatorium")).thenReturn(new Station("Sanatorium"));
        Barrier b = Barrier.build(n,s,p);
        Assert.assertNotNull(b);
    }
    @Test
    public void testBuild_NetworkNull(){
        String s = "Sanatorium";
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        Set<String> lines = new HashSet<String>();
        lines.add("L7");
        Barrier b = Barrier.build(null,s,p);
        Assert.assertNull(b);
    }
    @Test
    public void testBuild_StationNull(){
        Network n = Mockito.mock(Network.class);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        Set<String> lines = new HashSet<String>();
        lines.add("L7");
        Barrier b = Barrier.build(n,null,p);
        Assert.assertNull(b);
    }
    @Test
    public void testBuild_PricesNull(){
        Network n = Mockito.mock(Network.class);
        String s = "Sanatorium";
        Set<String> lines = new HashSet<String>();
        lines.add("L7");
        Barrier b = Barrier.build(n,s,null);
        Assert.assertNull(b);
    }
    @Test
    public void testBuild_NetworkHaveZeroLine(){
        Network n = Mockito.mock(Network.class);
        String s = "Sanatorium";
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        Set<String> lines = new HashSet<String>();
        Barrier b = Barrier.build(n,s,p);
        Assert.assertNull(b);
    }
    @Test
    public void testBuild_StationDoesNotBelongToNetwork(){
        Network n = Mockito.mock(Network.class);
        String s = "Sanatorium";
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        Set<String> lines = new HashSet<String>();
        lines.add("L7");
        Mockito.when(n.getLines()).thenReturn(lines);
        Mockito.when(n.getStationByName("Sanatorium")).thenReturn(null);
        Barrier b = Barrier.build(n,s,p);
        Assert.assertNull(b);
    }
    @Test
    public void testBuild_PricesEmpty(){
        Network n = Mockito.mock(Network.class);
        String s = "Sanatorium";
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        Set<String> lines = new HashSet<String>();
        lines.add("L7");
        Mockito.when(n.getLines()).thenReturn(lines);
        Mockito.when(n.getStationByName("Sanatorium")).thenReturn(new Station("Sanatorium"));
        Barrier b = Barrier.build(n,s,p);
        Assert.assertNull(b);
    }
    @Test
    public void testBuild_NoPriceAreaOfZero(){
        Network n = Mockito.mock(Network.class);
        String s = "Sanatorium";
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(3.7,1000);
        Set<String> lines = new HashSet<String>();
        lines.add("L7");
        Mockito.when(n.getLines()).thenReturn(lines);
        Mockito.when(n.getStationByName("Sanatorium")).thenReturn(new Station("Sanatorium"));
        Barrier b = Barrier.build(n,s,p);
        Assert.assertNull(b);
    }
    @Test
    public void testBuild_PriceAreaDistanceNegative(){
        Network n = Mockito.mock(Network.class);
        String s = "Sanatorium";
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(-1.0,1000);
        Set<String> lines = new HashSet<String>();
        lines.add("L7");
        Mockito.when(n.getLines()).thenReturn(lines);
        Mockito.when(n.getStationByName("Sanatorium")).thenReturn(new Station("Sanatorium"));
        Barrier b = Barrier.build(n,s,p);
        Assert.assertNull(b);
    }
    @Test
    public void testBuild_PriceAreaPriceNegative(){
        Network n = Mockito.mock(Network.class);
        String s = "Sanatorium";
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,-1000);
        Set<String> lines = new HashSet<String>();
        lines.add("L7");
        Mockito.when(n.getLines()).thenReturn(lines);
        Mockito.when(n.getStationByName("Sanatorium")).thenReturn(new Station("Sanatorium"));
        Barrier b = Barrier.build(n,s,p);
        Assert.assertNull(b);
    }
    @Test
    public void testBuild_PriceAreaPriceZero(){
        Network n = Mockito.mock(Network.class);
        String s = "Sanatorium";
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,0);
        Set<String> lines = new HashSet<String>();
        lines.add("L7");
        Mockito.when(n.getLines()).thenReturn(lines);
        Mockito.when(n.getStationByName("Sanatorium")).thenReturn(new Station("Sanatorium"));
        Barrier b = Barrier.build(n,s,p);
        Assert.assertNull(b);
    }

    //TEST de la fonction enter()
    @Test
    public void testEnter_Simple(){
        Network n = Mockito.mock(Network.class);
        String s = "Sanatorium";
        ITicket t = Mockito.mock(ITicket.class);
        //Mockito.when(t.isValid()).thenReturn(true);
        //Mockito.when((t.getEntryStation())).thenReturn(null);
        Mockito.when(t.getAmount()).thenReturn(1);
        Mockito.when(t.entering(s)).thenReturn(true);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        Set<String> lines = new HashSet<String>();
        lines.add("L7");
        Mockito.when(n.getLines()).thenReturn(lines);
        Mockito.when(n.getStationByName("Sanatorium")).thenReturn(new Station("Sanatorium"));
        Barrier b = Barrier.build(n,s,p);
        Assert.assertNotNull(b);
        Assert.assertTrue(b.enter(t));
    }
    @Test
    public void testEnter_TicketUnvalid(){
        Network n = Mockito.mock(Network.class);
        String s = "Sanatorium";
        ITicket t = Mockito.mock(ITicket.class);
        //Mockito.when(t.isValid()).thenReturn(false);
        //Mockito.when((t.getEntryStation())).thenReturn(null);
        //Mockito.when(t.getAmount()).thenReturn(1);
        //Mockito.when(t.entering(s)).thenReturn(false);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        Set<String> lines = new HashSet<String>();
        lines.add("L7");
        Mockito.when(n.getLines()).thenReturn(lines);
        Mockito.when(n.getStationByName("Sanatorium")).thenReturn(new Station("Sanatorium"));
        Barrier b = Barrier.build(n,s,p);
        Assert.assertNotNull(b);
        Assert.assertFalse(b.enter(t));
    }
    @Test
    public void testEnter_TicketEntered(){
        Network n = Mockito.mock(Network.class);
        String s = "Sanatorium";
        ITicket t = Mockito.mock(ITicket.class);
        //Mockito.when(t.isValid()).thenReturn(true);
        //Mockito.when((t.getEntryStation())).thenReturn("Sanatorium");
        //Mockito.when(t.getAmount()).thenReturn(1);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        Set<String> lines = new HashSet<String>();
        lines.add("L7");
        Mockito.when(n.getLines()).thenReturn(lines);
        Mockito.when(n.getStationByName("Sanatorium")).thenReturn(new Station("Sanatorium"));
        Barrier b = Barrier.build(n,s,p);
        Assert.assertNotNull(b);
        Assert.assertFalse(b.enter(t));
    }
    @Test
    public void testEnter_TicketAmountZero(){
        Network n = Mockito.mock(Network.class);
        String s = "Sanatorium";
        ITicket t = Mockito.mock(ITicket.class);
        //Mockito.when(t.isValid()).thenReturn(true);
        //Mockito.when((t.getEntryStation())).thenReturn(null);
        Mockito.when(t.getAmount()).thenReturn(0);
        Map<Double,Integer> p = new HashMap<Double,Integer>();
        p.put(0.0,10);
        Set<String> lines = new HashSet<String>();
        lines.add("L7");
        Mockito.when(n.getLines()).thenReturn(lines);
        Mockito.when(n.getStationByName("Sanatorium")).thenReturn(new Station("Sanatorium"));
        Barrier b = Barrier.build(n,s,p);
        Assert.assertNotNull(b);
        Assert.assertFalse(b.enter(t));
    }
}
