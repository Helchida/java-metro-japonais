package fr.ufc.l3info.oprog;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class TestAdjustedTicket {
    //Test isChild
    @Test
    public void isChild_IsChild(){
        ITicket t = new BaseTicket(true,0);
        AdjustedTicket at = new AdjustedTicket(t,10);
        Assert.assertTrue(at.isChild());
    }
    @Test
    public void isChild_IsNotChild(){
        ITicket t = new BaseTicket(false,0);
        AdjustedTicket at = new AdjustedTicket(t,10);
        Assert.assertFalse(at.isChild());
    }

    //Test getAmount à rajouter selon l'état du ticket
    @Test
    public void getAmount_Zero(){
        ITicket t = new BaseTicket(true,0);
        AdjustedTicket at = new AdjustedTicket(t,10);
        Assert.assertEquals(at.getAmount(),10);
    }
    @Test
    public void getAmount_NegativeValueConstructor(){
        ITicket t = new BaseTicket(true,-1);
        AdjustedTicket at = new AdjustedTicket(t,10);
        Assert.assertEquals(at.getAmount(),10);
    }
    @Test
    public void getAmount_SupZero(){
        ITicket t = new BaseTicket(true,12);
        AdjustedTicket at = new AdjustedTicket(t,10);
        Assert.assertEquals(at.getAmount(),22);
    }

    //Test entering
    @Test
    public void entering_issued_ParamValid(){
        ITicket t = new BaseTicket(true,12);
        AdjustedTicket at = new AdjustedTicket(t,10);
        Assert.assertTrue(at.entering("Sanatorium"));
        Assert.assertTrue(at.isValid() && at.getEntryStation()=="Sanatorium");
    }
    @Test
    public void entering_issued_StringNull(){
        ITicket t = new BaseTicket(true,12);
        AdjustedTicket at = new AdjustedTicket(t,10);
        Assert.assertFalse(at.entering(null));
        Assert.assertFalse(at.isValid());
    }
    @Test
    public void entering_issued_StringVoid_KO(){
        ITicket t = new BaseTicket(true,12);
        AdjustedTicket at = new AdjustedTicket(t,10);
        Assert.assertFalse(at.entering("")); // Erreur dans l'implémentation  resultat attendu FALSE
        Assert.assertFalse(at.isValid());
    }
    @Test
    public void entering_entered(){
        ITicket t = new BaseTicket(true,12);
        AdjustedTicket at = new AdjustedTicket(t,10);
        at.entering("Sanatorium");
        Assert.assertFalse(at.entering("UFR Sciences"));
        Assert.assertFalse(at.isValid());
    }
    @Test
    public void entering_invalid(){
        ITicket t = new BaseTicket(true,12);
        AdjustedTicket at = new AdjustedTicket(t,10);
        at.invalidate();
        Assert.assertFalse(at.entering("UFR Sciences"));
        Assert.assertFalse(at.isValid());
    }

    //Test invalidate
    @Test
    public void invalidate_Issued(){
        ITicket t = new BaseTicket(true,12);
        AdjustedTicket at = new AdjustedTicket(t,10);
        at.invalidate();
        Assert.assertFalse(at.isValid());
    }
    @Test
    public void invalidate_Entered(){
        ITicket t = new BaseTicket(true,12);
        AdjustedTicket at = new AdjustedTicket(t,10);
        at.entering("Sanatorium");
        at.invalidate();
        Assert.assertFalse(at.isValid());
    }
    @Test
    public void invalidate_Invalid(){
        ITicket t = new BaseTicket(true,12);
        AdjustedTicket at = new AdjustedTicket(t,10);
        at.invalidate();
        at.invalidate();
        Assert.assertFalse(at.isValid());
    }

    //Test getEntryStation
    @Test
    public void getEntryStation_Issued(){
        ITicket t = new BaseTicket(true,12);
        AdjustedTicket at = new AdjustedTicket(t,10);
        Assert.assertNull(at.getEntryStation());
    }
    @Test
    public void getEntryStation_Entered(){
        ITicket t = new BaseTicket(true,12);
        AdjustedTicket at = new AdjustedTicket(t,10);
        at.entering("Sanatorium");
        Assert.assertEquals(at.getEntryStation(),"Sanatorium");
    }
    @Test
    public void getEntryStation_Invalid(){
        ITicket t = new BaseTicket(true,12);
        AdjustedTicket at = new AdjustedTicket(t,10);
        at.invalidate();
        Assert.assertNull(at.getEntryStation());
    }
    @Test
    public void getEntryStation_IssuedThenEnteredThenInvalid(){
        ITicket t = new BaseTicket(true,12);
        AdjustedTicket at = new AdjustedTicket(t,10);
        at.entering("Sanatorium");
        at.entering("Sanatorium");
        Assert.assertNull(at.getEntryStation());
    }

    //Test isValid
    @Test
    public void isValid_Issued(){
        ITicket t = new BaseTicket(true,12);
        AdjustedTicket at = new AdjustedTicket(t,10);
        Assert.assertTrue(at.isValid());
    }
    @Test
    public void isValid_Entered(){
        ITicket t = new BaseTicket(true,12);
        AdjustedTicket at = new AdjustedTicket(t,10);
        at.entering("Sanatorium");
        Assert.assertTrue(at.isValid());
    }
    @Test
    public void isValid_Invalid(){
        ITicket t = new BaseTicket(true,12);
        AdjustedTicket at = new AdjustedTicket(t,10);
        at.invalidate();
        Assert.assertFalse(at.isValid());
    }
    @Test
    public void isValid_IssuedThenEnteredThenInvalid(){
        ITicket t = new BaseTicket(true,12);
        AdjustedTicket at = new AdjustedTicket(t,10);
        at.entering("Sanatorium");
        at.entering("Sanatorium");
        Assert.assertFalse(at.isValid());
    }

    //Test Constructeur nouvelle class
    @Test(expected = NullPointerException.class)
    public void constructor_voidITicket(){
        AdjustedTicket at = new AdjustedTicket(null,10);
    }
    @Test
    public void constructor_NegativeAmount(){
        ITicket t = new BaseTicket(true,12);
        AdjustedTicket at = new AdjustedTicket(t,-10);
        Assert.assertEquals(12, at.getAmount());
    }
}

