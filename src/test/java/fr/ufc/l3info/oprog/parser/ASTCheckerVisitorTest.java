package fr.ufc.l3info.oprog.parser;
import fr.ufc.l3info.oprog.Network;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class ASTCheckerVisitorTest {
    /** Chemin vers les fichiers de test */
    final String path = "./target/classes/data/";

    /** Instance singleton du parser de stations */
    final NetworkParser parser = NetworkParser.getInstance();


    @Test
    public void testCheckerVisitorNoError() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroOK.txt"));
        ASTCheckerVisitor visitor = new ASTCheckerVisitor();
        n.accept(visitor);

        Map<String,ERROR_KIND> map=visitor.getErrors();
        for(Map.Entry mapentry : map.entrySet()){
            System.out.println("Clé : "+mapentry.getKey()
                    + " | Valeur : " + mapentry.getValue());
        }

        Assert.assertEquals(0,map.size());

    }

    @Test
    public void testCheckerVisitorNoError2() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroOK2.txt"));
        ASTCheckerVisitor visitor = new ASTCheckerVisitor();
        n.accept(visitor);

        Map<String,ERROR_KIND> map=visitor.getErrors();
        for(Map.Entry mapentry : map.entrySet()){
            System.out.println("Clé : "+mapentry.getKey()
                    + " | Valeur : " + mapentry.getValue());
        }

        Assert.assertEquals(0,map.size());

    }

    @Test
    public void testCheckerVisitorEmptyAll() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroEmpty.txt"));
        ASTCheckerVisitor visitor = new ASTCheckerVisitor();
        n.accept(visitor);

        Map<String,ERROR_KIND> map=visitor.getErrors();
        for(Map.Entry mapentry : map.entrySet()){
            System.out.println("Clé : "+mapentry.getKey()
                    + " | Valeur : " + mapentry.getValue());
        }

        Assert.assertEquals(1,map.size());

    }

    @Test
    public void testCheckerVisitorEmptyNameLine() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroLineNameEmpty.txt"));
        ASTCheckerVisitor visitor = new ASTCheckerVisitor();
        n.accept(visitor);

        Map<String,ERROR_KIND> map=visitor.getErrors();
        for(Map.Entry mapentry : map.entrySet()){
            System.out.println("Clé : "+mapentry.getKey()
                                + " | Valeur : " + mapentry.getValue());
        }

        Assert.assertEquals(3,map.size());


    }

    @Test
    public void testCheckerVisitorSameNameLine() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroLineSameName.txt"));
        ASTCheckerVisitor visitor = new ASTCheckerVisitor();
        n.accept(visitor);

        Map<String,ERROR_KIND> map=visitor.getErrors();
        for(Map.Entry mapentry : map.entrySet()){
            System.out.println("Clé : "+mapentry.getKey()
                    + " | Valeur : " + mapentry.getValue());
        }

        Assert.assertEquals(1,map.size());

    }

    @Test
    public void testCheckerVisitorEmptyNameStation() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroStationNameEmpty.txt"));
        ASTCheckerVisitor visitor = new ASTCheckerVisitor();
        n.accept(visitor);

        Map<String,ERROR_KIND> map=visitor.getErrors();
        for(Map.Entry mapentry : map.entrySet()){
            System.out.println("Clé : "+mapentry.getKey()
                    + " | Valeur : " + mapentry.getValue());
        }

        Assert.assertEquals(3,map.size());
    }

    @Test
    public void testCheckerVisitorSameNameStation() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroStationSameName.txt"));
        ASTCheckerVisitor visitor = new ASTCheckerVisitor();
        n.accept(visitor);

        Map<String,ERROR_KIND> map=visitor.getErrors();
        for(Map.Entry mapentry : map.entrySet()){
            System.out.println("Clé : "+mapentry.getKey()
                    + " | Valeur : " + mapentry.getValue());
        }

        Assert.assertEquals(2,map.size());


    }

    @Test
    public void testCheckerVisitorNegativeNumber() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroStationNegativeNumber.txt"));
        ASTCheckerVisitor visitor = new ASTCheckerVisitor();
        n.accept(visitor);

        Map<String,ERROR_KIND> map=visitor.getErrors();
        for(Map.Entry mapentry : map.entrySet()){
            System.out.println("Clé : "+mapentry.getKey()
                    + " | Valeur : " + mapentry.getValue());
        }

        Assert.assertEquals(2,map.size());

    }

    @Test
    public void testCheckerVisitorNotZeroForFirst() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroStationNotZeroForFirst.txt"));
        ASTCheckerVisitor visitor = new ASTCheckerVisitor();
        n.accept(visitor);

        Map<String,ERROR_KIND> map=visitor.getErrors();
        for(Map.Entry mapentry : map.entrySet()){
            System.out.println("Clé : "+mapentry.getKey()
                    + " | Valeur : " + mapentry.getValue());
        }
        Assert.assertEquals(2,map.size());

    }

    @Test
    public void testCheckerVisitorNotCroissantNumber() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroStationNotCroissantNumber.txt"));
        ASTCheckerVisitor visitor = new ASTCheckerVisitor();
        n.accept(visitor);

        Map<String,ERROR_KIND> map=visitor.getErrors();
        for(Map.Entry mapentry : map.entrySet()){
            System.out.println("Clé : "+mapentry.getKey()
                    + " | Valeur : " + mapentry.getValue());
        }

        Assert.assertEquals(3,map.size());

    }

    @Test
    public void testCheckerVisitorNotAccessibleAllLine() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroLineNonAccessible.txt"));
        ASTCheckerVisitor visitor = new ASTCheckerVisitor();
        n.accept(visitor);

        Map<String,ERROR_KIND> map=visitor.getErrors();
        for(Map.Entry mapentry : map.entrySet()){
            System.out.println("Clé : "+mapentry.getKey()
                    + " | Valeur : " + mapentry.getValue());
        }

        Assert.assertEquals(1,map.size());


    }

    @Test
    public void testCheckerVisitorNotAccessibleOneLine() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroOneLineNonAccessible.txt"));
        ASTCheckerVisitor visitor = new ASTCheckerVisitor();
        n.accept(visitor);

        Map<String,ERROR_KIND> map=visitor.getErrors();
        for(Map.Entry mapentry : map.entrySet()){
            System.out.println("Clé : "+mapentry.getKey()
                    + " | Valeur : " + mapentry.getValue());
        }

        Assert.assertEquals(1,map.size());


    }


}
