package fr.ufc.l3info.oprog.parser;

import fr.ufc.l3info.oprog.Network;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

/**
 *  Quelques tests pour le package parser.
 */
public class NetworkParserTest {

    /** Chemin vers les fichiers de test */
    final String path = "./target/classes/data/";

    /** Instance singleton du parser de stations */
    final NetworkParser parser = NetworkParser.getInstance();

    @Test
    public void testTokenizer() throws NetworkParserException, IOException {
        List<Token> tokens = NetworkFileTokenizer.tokenize(new File(path + "metroOK.txt"));
        assertEquals(53, tokens.size());
        String[] expected = {
                "ligne", "\"line0\"", "{",
                "\"s0\"", ":", "0.0", ",", "\"s1\"", ":", "1", ",", "\"s2\"", ":", "4", ",", "\"s3\"", ":", "6", ",", "\"s4\"", ":", "7",
                "}",
                "ligne", "\"line1\"", "{",
                "\"s0\"", ":", "0.0", ",", "\"s3\"", ":", "3.0", ",", "\"s5\"", ":", "5.0",
                "}",
                "ligne", "\"line2\"", "{",
                "\"s1\"", ":", "0.0", ",", "\"s5\"", ":", "2", ",", "\"s4\"", ":", "5.0", "}"
        };
        for (int i=0; i < expected.length; i++) {
            assertEquals(expected[i], tokens.get(i).getValeur());
        }
        assertEquals(1, tokens.get(0).getLigne());
        assertEquals(1, tokens.get(0).getColonne());
        assertEquals(16, tokens.get(tokens.size()-1).getLigne());
        assertEquals(15, tokens.get(tokens.size()-1).getColonne());
    }


    @Test
    public void testParserOK() throws NetworkParserException, IOException {
        ASTNode n = parser.parse(new File(path + "metroOK.txt"));
        assertTrue(n instanceof ASTListeLignes);
        assertEquals(3, n.getNumChildren());

        assertEquals(6, n.getChild(0).getNumChildren());
        assertEquals(4, n.getChild(1).getNumChildren());
        assertEquals(4, n.getChild(2).getNumChildren());

        for (ASTNode n1 : n) {
            assertTrue(n1 instanceof ASTLigne);
            for (ASTNode nn1 : n1) {
                if (!(nn1 instanceof ASTChaine)) {
                    assertTrue(nn1 instanceof ASTDeclaration);
                    assertTrue(nn1.getChild(0) instanceof ASTChaine);
                    assertTrue(nn1.getChild(1) instanceof ASTNombre);
                }
            }
        }
    }


    @Test
    public void testNetworkBuilder() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroOK.txt"));
        NetworkBuilder builder = new NetworkBuilder();
        n.accept(builder);
        Network net = builder.getNetwork();
        assertEquals(3, net.getLines().size());
        assertNotNull(net.getStationByName("s3"));
        assertTrue(net.getStationByName("s3").getLines().contains("line0"));
        assertTrue(net.getStationByName("s3").getLines().contains("line1"));
    }

    @Test
    public void testNetworkEmpty() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroEmpty.txt"));
    }

    @Test(expected = NetworkParserException.class)
    public void testNetworkNoStationInLine() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroNoStation.txt"));
    }
    @Test(expected = NetworkParserException.class)
    public void testNetworkNoNumberInStation() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroNoNumber.txt"));
    }
    @Test(expected = NetworkParserException.class)
    public void testNetworkNoDeuxPointInStation() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroNoDeuxPoint.txt"));
    }

    @Test(expected = NetworkParserException.class)
    public void testNetworkNoVirguleInStation() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroNoVirguleInStation.txt"));
    }

    @Test(expected = NetworkParserException.class)
    public void testNetworkNoAccolFInLigne() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroNoAccolFInLigne.txt"));
    }

    @Test(expected = NetworkParserException.class)
    public void testNetworkNoAccolOInLigne() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroNoAccolOInLigne.txt"));
    }

    @Test(expected = NetworkParserException.class)
    public void testNetworkNoChaineInLigne() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroNoChaineInLigne.txt"));
    }
    @Test(expected = NetworkParserException.class)
    public void testNetworkNoLigneInLigne() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroNoLigneInLigne.txt"));
    }

    @Test(expected = NetworkParserException.class)
    public void testNetworkNoGuillemetOInStation() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroNoGuillemetOInStation.txt"));
    }

    @Test(expected = NetworkParserException.class)
    public void testNetworkNoGuillemetFInStation() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroNoGuillemetFInStation.txt"));
    }

    @Test(expected = NetworkParserException.class)
    public void testNetworkNoGuillemetOInLigne() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroNoGuillemetOInLigne.txt"));
    }

    @Test(expected = NetworkParserException.class)
    public void testNetworkNoGuillemetFInLigne() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroNoGuillemetFInLigne.txt"));
    }

    @Test(expected = NetworkParserException.class)
    public void testNetworkChaineInStationIsInt() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroChaineInStationIsInt.txt"));
    }

    @Test(expected = NetworkParserException.class)
    public void testNetworkChaineInLigneIsInt() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroChaineInLigneIsInt.txt"));
    }

    @Test(expected = NetworkParserException.class)
    public void testNetworkNumberInStationIsChaine() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroNumberInStationIsChaine.txt"));
    }

    @Test(expected = NetworkParserException.class)
    public void testNetworkLigneBadSyntax() throws IOException, NetworkParserException {
        ASTNode n = parser.parse(new File("./target/classes/data/metroLigneBadSyntax.txt"));
    }






}
