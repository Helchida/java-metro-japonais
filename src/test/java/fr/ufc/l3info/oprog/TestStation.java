package fr.ufc.l3info.oprog;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


/**
 * Test file for the Station class.
 */
public class TestStation {

    //Test constructeur & getname & getDistanceForLine & getNumberForLine
    @Test
    public void constructeur_WithOutLine(){
        Station s = new Station("Edison");
        Assert.assertNotNull(s);
        Assert.assertEquals(s.getName(),"Edison");
    }
    @Test
    public void constructeur_WithLine(){
        Station s = new Station("Edison","L10",2,3.7);
        Assert.assertNotNull(s);
        Assert.assertTrue(s.getLines().contains("L10"));
        Assert.assertEquals(s.getDistanceForLine("L10"),3.7,0.001);
        Assert.assertEquals(s.getNumberForLine("L10"),2);
    }


    //Test addLine
    @Test
    public void addLine_LineIsNotPresent() {
        Station s = new Station("Edison");
        s.addLine("L10",2,3.7);
        Assert.assertTrue(s.getLines().contains("L10"));
        Assert.assertEquals(s.getDistanceForLine("L10"),3.7,0.001);
        Assert.assertEquals(s.getNumberForLine("L10"),2);
        Assert.assertEquals(s.getDistanceForLine("L10"),3.7,0.001);
        Assert.assertEquals(s.getNumberForLine("L10"),2);
    }
    @Test
    public void addLine_LineAlreadyPresent() {
        Station s = new Station("Edison","L10",2,3.7);
        s.addLine("L10",3,4.7);
        Assert.assertTrue(s.getLines().contains("L10"));
        Assert.assertEquals(s.getDistanceForLine("L10"),4.7,0.001);
        Assert.assertEquals(s.getNumberForLine("L10"),3);
    }


    //Test removeLine
    @Test
    public void removeLine_LineIsPresent(){
        Station s = new Station("Edison","L10",2,3.7);
        s.removeLine("L10");
        Assert.assertFalse(s.getLines().contains("L10"));
        Assert.assertTrue(s.getDistanceForLine("L10")<0);
        Assert.assertEquals(s.getNumberForLine("L10"),0);
    }
    @Test
    public void removeLine_LineIsNotPresent(){
        Station s = new Station("Edison","L10",2,3.7);
        s.removeLine("L11");
        Assert.assertTrue(s.getLines().contains("L10"));
        Assert.assertEquals(s.getDistanceForLine("L10"),3.7,0.001);
        Assert.assertEquals(s.getNumberForLine("L10"),2);
    }

    //Test equals
    @Test
    public void equals_StrictEquals(){
        Station s1 = new Station("Edison","L10",2,3.7);
        Station s2 = new Station("Edison","L10",2,3.7);
        Assert.assertEquals(s1, s2);
    }
    @Test
    public void equals_JustName(){
        Station s1 = new Station("Edison","L10",2,3.7);
        Station s2 = new Station("Edison","L11",3,4.7);
        Assert.assertEquals(s1, s2);
    }
    @Test
    public void equals_Nothing(){
        Station s1 = new Station("Edison","L10",2,3.7);
        Station s2 = new Station("Sanatorium","L9",3,4.7);
        Assert.assertNotEquals(s1, s2);
    }

    //Test Hash
    @Test
    public void hash_sameName(){
        Station s1 = new Station("Edison","L10",2,3.7);
        Station s2 = new Station("Edison","L10",2,3.7);
        Assert.assertEquals(s1.hashCode(), s2.hashCode());
    }
}
